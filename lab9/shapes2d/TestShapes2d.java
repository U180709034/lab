package shapes2d;

public class TestShapes2d {
    public static void main(String[] args){
        Circle circle= new Circle(5);
        Square square= new Square(4);

        System.out.println(circle.toString());
        System.out.println(square.toString());

        System.out.println("Circle Area = " + circle.area());
    }
}
