package shapes3d;

public class TestShapes3d {

    public static void main(String[] args){
        Cylinder cylinder = new Cylinder(5,10);
        System.out.println(cylinder.toString());

        System.out.println("Cylinder volume = " + cylinder.volume());
        System.out.println("Cylinder area = " + cylinder.area());

        Cube cube = new Cube(4);
        System.out.println(cube);

        System.out.println("Cube volume = " + cube.volume());
        System.out.println("Cube area = " + cube.area());


    }


}
